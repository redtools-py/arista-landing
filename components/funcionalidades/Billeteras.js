import ZoomSlider from '../../lib/react-instagram-zoom-slider'

export default function Billeteras () {
  const billeterasSlidesData = [
    {
      url: 'images/billeteras/lista-de-movimientos.jpg',
      title: 'Lista de movimientos en billeteras'
    },
    {
      url: 'images/billeteras/extracción-de-dinero.jpg',
      title: 'Extracción de dinero'
    },
    {
      url: 'images/billeteras/ficha-de-movimientos.jpg',
      title: 'Ficha de movimientos'
    }
  ]
  const billeterasSlides = billeterasSlidesData.map(src => <img src={src.url} alt={src.title} className="lg:rounded-t-lg" draggable="false" />)

  return (
    <section id="billeteras" className="py-20 lg:py-32 bg-gray-200">
      <div className="lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto lg:flex-row-reverse">
        <div className="px-6 sm:px-20 text-left lg:w-1/2">
          <h4 className="uppercase font-bold tracking-wide text-xl text-blue-700">Billeteras</h4>
          <h3 className="lg:pb-10 text-4xl lg:text-5xl font-thin leading-tight text-blue-700">
            Controla gastos e <br className="hidden lg:block" /> ingresos varios
              </h3>
          <div className="my-10 lg:mt-0 text-gray-600 font-light leading-relaxed text-lg lg:text-xl">
            <div className="relative">
              <svg className="absolute pt-3 fill-current text-blue-500 w-auto h-10 mx-auto" viewBox="0 0 512 512">
                <path d="M221.843,420.255C119.875,415.957,40,332.574,40,230.427c0-104.766,85.234-190,190-190				c88.342,0,166.346,62.489,185.476,148.583c2.396,10.783,13.076,17.581,23.862,15.186c10.783-2.396,17.582-13.079,15.186-23.862				c-11.17-50.266-39.456-95.921-79.649-128.553C334.085,18.665,282.634,0.427,230,0.427c-61.436,0-119.193,23.924-162.635,67.365				C23.924,111.234,0,168.991,0,230.427c0,59.719,22.784,116.314,64.153,159.358c41.249,42.918,96.652,67.933,156.005,70.435				c0.287,0.012,0.572,0.018,0.857,0.018c10.658,0,19.515-8.409,19.968-19.158C241.449,430.044,232.879,420.72,221.843,420.255z" />			<path d="M230,80.574c-11.046,0-20,8.954-20,20v121.569l-64.613,64.613c-7.811,7.81-7.811,20.473,0,28.284				c3.905,3.905,9.023,5.858,14.142,5.858s10.237-1.953,14.142-5.858l70.471-70.471c3.751-3.75,5.858-8.838,5.858-14.142V100.574				C250,89.528,241.046,80.574,230,80.574z" />			<path d="M470.855,248.773c-20.853-8.511-48.147-13.199-76.854-13.199c-28.707,0-56.001,4.688-76.854,13.199				c-35.8,14.612-41.146,35.595-41.146,46.801v156c0,11.207,5.346,32.189,41.146,46.801c20.853,8.511,48.147,13.199,76.854,13.199				c28.707,0,56.001-4.688,76.854-13.199c35.8-14.612,41.146-35.595,41.146-46.801v-156				C512.001,284.367,506.655,263.385,470.855,248.773z M472.001,450.94c-3.304,6.149-30.193,20.634-78,20.634				s-74.696-14.484-78-20.634v-32.052c0.385,0.162,0.753,0.326,1.146,0.487c20.853,8.511,48.147,13.199,76.854,13.199				c6.924,0,13.855-0.274,20.6-0.815c11.01-0.883,19.22-10.525,18.336-21.536c-0.884-11.011-10.512-19.216-21.536-18.336				c-5.685,0.456-11.539,0.688-17.4,0.688c-47.82,0-74.708-14.49-78-20.637V341.89c0.385,0.162,0.753,0.326,1.146,0.487				c20.853,8.511,48.147,13.199,76.854,13.199c6.924,0,13.855-0.274,20.6-0.815c11.01-0.883,19.22-10.525,18.336-21.536				c-0.884-11.011-10.512-19.217-21.536-18.336c-5.685,0.456-11.539,0.688-17.4,0.688c-46.242,0-72.907-13.549-77.594-20.001				c4.694-6.454,31.36-19.999,77.594-19.999c47.807,0,74.696,14.484,78,20.634V450.94z" />
              </svg>
              <p className="pl-16">
                Agiliza el control de tus números registrando
                los gastos e ingresos varios.
              </p>
            </div>

            <div className="mt-6 relative">
              <svg className="absolute pt-3 fill-current text-blue-500 w-auto h-10 mx-auto" viewBox="0 0 512 512">
                <path d="M20,106h233c11.046,0,20-8.954,20-20s-8.954-20-20-20H20C8.954,66,0,74.954,0,86S8.954,106,20,106z" />			<path d="M373,171c33.084,0,60-26.916,60-60v-5h59c11.046,0,20-8.954,20-20s-8.954-20-20-20h-59v-6c0-33.084-26.916-60-60-60				s-60,26.916-60,60v51C313,144.084,339.916,171,373,171z M353,60c0-11.028,8.972-20,20-20s20,8.972,20,20v51				c0,11.028-8.972,20-20,20s-20-8.972-20-20V60z" />			<path d="M252.954,407H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h232.954c11.046,0,20-8.954,20-20				C272.954,415.954,264,407,252.954,407z" />			<path d="M20,275.878h57c11.046,0,20-8.954,20-20s-8.954-20-20-20H20c-11.046,0-20,8.954-20,20S8.954,275.878,20,275.878z" />			<path d="M492,235.878H257V229c0-33.084-26.916-60-60-60s-60,26.916-60,60v52c0,33.084,26.916,60,60,60s60-26.916,60-60v-5.122				h235c11.046,0,20-8.954,20-20C512,244.833,503.046,235.878,492,235.878z M217,281c0,11.028-8.972,20-20,20s-20-8.972-20-20v-52				c0-11.028,8.972-20,20-20s20,8.972,20,20V281z" />			<path d="M492,407h-59.046v-6c0-33.084-26.916-60-60-60c-33.084,0-60,26.916-60,60v51c0,33.084,26.916,60,60,60				c33.084,0,60-26.916,60-60v-5H492c11.046,0,20-8.954,20-20C512,415.954,503.046,407,492,407z M392.954,452				c0,11.028-8.972,20-20,20c-11.028,0-20-8.972-20-20v-51c0-11.028,8.972-20,20-20c11.028,0,20,8.972,20,20V452z" />
              </svg>
              <p className="pl-16">
                Agrupa las operaciones por billeteras y tipos de movimientos.
              </p>
            </div>
          </div>
        </div>
        <div className="p-3 lg:w-1/2">
          <div className="bg-white border border-gray-300 shadow-lg rounded-lg lg:ml-10">
            <ZoomSlider
              slides={billeterasSlides}
              slidesData={billeterasSlidesData}
              slideOverlayBottom={(currentSlide) => {
                return <div className='font-semibold my-4 mx-2 text-center'>{billeterasSlidesData[currentSlide].title}</div>
              }}
              slideIndicatorTimeout={null}
              showButtons={true}
              activeDotColor="#409cff"
              dotColor="#ccc" />
          </div>
        </div>
      </div>
    </section>
  )
}