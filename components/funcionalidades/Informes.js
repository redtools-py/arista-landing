import ZoomSlider from '../../lib/react-instagram-zoom-slider'

export default function Informes () {
  const informesSlidesData = [
    {
      url: 'images/informes/balance.jpg',
      title: 'Balance mensual'
    },
    {
      url: 'images/informes/detalles-de-iva.jpg',
      title: 'Detalles del IVA'
    },
    {
      url: 'images/informes/totales-de-productos-vendidos.jpg',
      title: 'Totales de ventas por producto'
    },
    {
      url: 'images/informes/exportar-listas.jpg',
      title: 'Exportar listas filtradas'
    },
    {
      url: 'images/productos/lista-de-productos-filtrada.jpg',
      title: 'Listas filtradas'
    }
  ]
  const informesSlides = informesSlidesData.map(src => <img src={src.url} alt={src.title} className="lg:rounded-t-md" draggable="false" />)

  return (
    <section id="informes" className="py-20 lg:py-32 bg-gray-200">
      <div className="lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto lg:flex-row-reverse">
        <div className="px-6 sm:px-20 text-left lg:w-1/2">
          <h4 className="uppercase font-bold tracking-wide text-xl text-blue-700">Informes</h4>
          <h3 className="lg:pb-10 text-4xl lg:text-5xl font-thin leading-tight text-blue-700">
            Ten el poder de tus datos a tu favor
          </h3>
          <div className="my-10 lg:mt-0 text-gray-600 font-light leading-relaxed text-lg lg:text-xl">
            <div className="relative">
              <svg className="absolute pt-3 fill-current text-blue-500 w-auto h-10 mx-auto" viewBox="0 0 512 512">
                <path d="M45.33,283.02l-39.696,40.92c-7.675,7.913-7.484,20.55,0.429,28.226c3.878,3.763,8.889,5.634,13.897,5.634				c5.21,0,10.416-2.028,14.329-6.062l39.696-40.92c7.675-7.913,7.484-20.55-0.429-28.226				C65.643,274.915,53.006,275.107,45.33,283.02z" />			<path d="M137.731,295.92c-11.023,0-19.961,8.937-19.961,19.961v175.658c0,11.023,8.937,19.961,19.961,19.961h0.001				c11.023,0,19.961-8.937,19.961-19.961l-0.002-175.659C157.691,304.857,148.755,295.92,137.731,295.92z" />			<path d="M215.579,252.007c-11.023,0-19.961,8.937-19.961,19.961v219.571c0,11.023,8.937,19.961,19.961,19.961				s19.961-8.937,19.961-19.961V271.967C235.539,260.944,226.603,252.007,215.579,252.007z" />			<path d="M452.116,0.498h-59.883c-11.024,0-19.961,8.937-19.961,19.961s8.937,19.961,19.961,19.961h52.61L298.273,186.959				c-3.932,3.933-9.078,6.17-14.167,6.17c-0.107,0-0.216-0.001-0.323-0.003c-4.901-0.092-9.91-2.339-13.738-6.168l-23.468-23.468				c-11.489-11.489-26.741-18.149-41.848-18.273c-0.147-0.001-0.293-0.001-0.439-0.001c-15.636,0-31.25,6.835-42.919,18.799				l-42.776,42.969c-7.778,7.813-7.75,20.451,0.063,28.229c3.894,3.877,8.988,5.815,14.082,5.815c5.123,0,10.246-1.961,14.146-5.879				l42.854-43.046c0.059-0.058,0.116-0.117,0.174-0.176c4.168-4.293,9.6-6.839,14.488-6.79c4.651,0.038,9.865,2.498,13.948,6.581				l23.468,23.468c11.226,11.227,25.866,17.568,41.223,17.854c15.969,0.3,31.822-6.209,43.463-17.852L472.077,69.644v49.622				c0,11.024,8.937,19.961,19.961,19.961s19.961-8.937,19.961-19.961V60.381C511.999,27.362,485.136,0.498,452.116,0.498z" />			<path d="M58.952,385.746c-11.05-0.038-20.029,8.91-20.029,19.961v85.832c0,11.023,8.937,19.961,19.961,19.961h0.001				c11.024,0,19.961-8.937,19.961-19.961v-85.832C78.845,394.709,69.951,385.783,58.952,385.746z" />			<path d="M451.118,171.165c-11.024,0-19.961,8.937-19.961,19.961V491.54c0,11.023,8.937,19.961,19.961,19.961h0.001				c11.023,0,19.961-8.937,19.961-19.961V191.126C471.079,180.101,462.143,171.165,451.118,171.165z" />			<path d="M293.426,276.958c-11.024,0-19.961,8.937-19.961,19.961v194.62c0,11.023,8.937,19.961,19.961,19.961h0.001				c11.023,0,19.961-8.937,19.961-19.961v-194.62C313.387,285.895,304.451,276.958,293.426,276.958z" />			<path d="M372.2,239.032c-10.996,0.04-19.888,8.966-19.888,19.961V491.54c0,11.023,8.937,19.961,19.961,19.961h0.001				c11.023,0,19.961-8.937,19.961-19.961V258.993C392.233,247.94,383.252,238.991,372.2,239.032z" />
              </svg>
              <p className="pl-16">
                Aprovecha los cálculos automáticos y totales obtenidos
                a partir de tus operaciones registradas.
              </p>
            </div>

            <div className="mt-6 relative">
              <svg className="absolute pt-3 fill-current text-blue-500 w-auto h-10 mx-auto" viewBox="0 0 512 512">
                <path d="M20,106h233c11.046,0,20-8.954,20-20s-8.954-20-20-20H20C8.954,66,0,74.954,0,86S8.954,106,20,106z" />			<path d="M373,171c33.084,0,60-26.916,60-60v-5h59c11.046,0,20-8.954,20-20s-8.954-20-20-20h-59v-6c0-33.084-26.916-60-60-60				s-60,26.916-60,60v51C313,144.084,339.916,171,373,171z M353,60c0-11.028,8.972-20,20-20s20,8.972,20,20v51				c0,11.028-8.972,20-20,20s-20-8.972-20-20V60z" />			<path d="M252.954,407H20c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h232.954c11.046,0,20-8.954,20-20				C272.954,415.954,264,407,252.954,407z" />			<path d="M20,275.878h57c11.046,0,20-8.954,20-20s-8.954-20-20-20H20c-11.046,0-20,8.954-20,20S8.954,275.878,20,275.878z" />			<path d="M492,235.878H257V229c0-33.084-26.916-60-60-60s-60,26.916-60,60v52c0,33.084,26.916,60,60,60s60-26.916,60-60v-5.122				h235c11.046,0,20-8.954,20-20C512,244.833,503.046,235.878,492,235.878z M217,281c0,11.028-8.972,20-20,20s-20-8.972-20-20v-52				c0-11.028,8.972-20,20-20s20,8.972,20,20V281z" />			<path d="M492,407h-59.046v-6c0-33.084-26.916-60-60-60c-33.084,0-60,26.916-60,60v51c0,33.084,26.916,60,60,60				c33.084,0,60-26.916,60-60v-5H492c11.046,0,20-8.954,20-20C512,415.954,503.046,407,492,407z M392.954,452				c0,11.028-8.972,20-20,20c-11.028,0-20-8.972-20-20v-51c0-11.028,8.972-20,20-20c11.028,0,20,8.972,20,20V452z" />
              </svg>
              <p className="pl-16">
                Encuentra la información que necesitas de manera rápida
                utilizando los filtros disponibles en los listados.
              </p>
            </div>

            <div className="mt-6 relative">
              <svg className="absolute pt-3 fill-current text-blue-500 w-auto h-10 mx-auto" viewBox="0 0 512 512">
                <path d="M492,352c11.046,0,20-8.954,20-20V80c0-44.112-35.888-80-80-80H80C35.888,0,0,35.888,0,80v352c0,44.112,35.888,80,80,80			h352c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20c0,22.056-17.944,40-40,40h-76v-78h36			c11.046,0,20-8.954,20-20c0-11.046-8.954-20-20-20h-36v-78h116v56C472,343.046,480.954,352,492,352z M156,472H80			c-22.056,0-40-17.944-40-40v-38h116V472z M156,354H40v-78h116V354z M156,236H40v-78h116V236z M156,118H40V80			c0-22.056,17.944-40,40-40h76V118z M316,472H196v-78h120V472z M316,354H196v-78h120V354z M316,236H196v-78h120V236z M316,118H196			V40h120V118z M356,40h76c22.056,0,40,17.944,40,40v38H356V40z M356,236v-78h116v78H356z" />
              </svg>
              <p className="pl-16">
                Exporta las listas filtradas a excel para obtener
                cálculos personalizados.
              </p>
            </div>

            <div className="mt-6 relative">
              <svg className="absolute pt-3 fill-current text-blue-500 w-auto h-10 mx-auto" viewBox="0 0 512 512">
                <path d="M301,392h-90c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h90c11.046,0,20-8.954,20-20				C321,400.954,312.046,392,301,392z" />			<circle cx="421" cy="259" r="20" />			<path d="M492,312c11.046,0,20-8.954,20-20v-43c0-44.112-35.888-80-80-80h-31V20c0-11.046-8.954-20-20-20H131				c-11.046,0-20,8.954-20,20v149H80c-44.112,0-80,35.888-80,80v143c0,44.112,35.888,80,80,80h31v20c0,11.046,8.954,20,20,20h250				c11.046,0,20-8.954,20-20v-20h31c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20				c0,22.056-17.944,40-40,40h-31v-83h11c11.046,0,20-8.954,20-20c0-11.046-8.954-20-20-20H100c-11.046,0-20,8.954-20,20				c0,11.046,8.954,20,20,20h11v83H80c-22.056,0-40-17.944-40-40V249c0-22.056,17.944-40,40-40h352c22.056,0,40,17.944,40,40v43				C472,303.046,480.954,312,492,312z M151,349h210v123H151V349z M361,169H151V40h210V169z" />
              </svg>
              <p className="pl-16">
                También puedes imprimir las listas filtradas.
              </p>
            </div>
          </div>
        </div>
        <div className="p-3 lg:w-1/2">
          <div className="bg-white border border-gray-300 shadow-lg rounded-lg lg:ml-10">
            <ZoomSlider
              slides={informesSlides}
              slidesData={informesSlidesData}
              slideOverlayBottom={(currentSlide) => {
                return <div className='font-semibold my-4 mx-2 text-center'>{informesSlidesData[currentSlide].title}</div>
              }}
              slideIndicatorTimeout={null}
              showButtons={true}
              activeDotColor="#409cff"
              dotColor="#ccc" />
          </div>
        </div>
      </div>
    </section>
  )
}