import Head from 'next/head'
import NavbarHome from './navbarHome'
import styles from './layout.module.css'
import Link from 'next/link'

const name = 'Arista'
export const siteTitle = 'Arista | Tecnología para negocios'

export default function Layout({ children, home, isSticky }) {
  return (
    <div className="overflow-hidden">
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Sistema informático de facturación optimizado para tu economía y la seguridad de tus datos."
        />
        {/* <meta
          property="og:image"
          content={`https://og-image.now.sh/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        /> */}
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <NavbarHome isSticky={isSticky}></NavbarHome>
      <main>{children}</main>
    </div>
  )
}